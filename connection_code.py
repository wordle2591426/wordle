import requests as rq
import random
from itertools import permutations as nPr

class Wordle_Bot:
    mm_url = "https://we6.talentsprint.com/wordle/game/"

    def __init__(self, name, mode="wordle"):
        self.name = name
        self.mode = mode
        self.session = rq.Session()
        self.id = None

    def post(self, task, data):
        try:
            response = self.session.post(self.mm_url + task, json=data)
            response.raise_for_status()
            return response.json()
        except rq.exceptions.HTTPError as e:
            print(f"An error occurred during {task}: {e}")
            return None

    def register(self):
        response = self.post("register", {"mode": self.mode, "name": self.name})
        if response:
            self.id = response["id"]
            print(f"Registered successfully with ID: {self.id}")

    def create_game(self):
        if not self.id:
            print("You need to register first to create a game.")
            return
        self.post("create", {"id": self.id, "overwrite": True})
        print("Game created successfully.")

    def make_guess(self, guess_word):
        if not self.id:
            print("You need to register and create a game first to make a guess.")
            return
        response = self.post("guess", {"id": self.id, "guess": guess_word})
        if response:
            feedback = response["feedback"]
            message = response["message"]
            print(f"Your guess: {guess_word} \nFeedback: {feedback} \nMessage: {message}")
            return feedback, message
        return None, None

class Game:
    def __init__(self, file_path):
        self.possible_words = self.load_data(file_path)

    def load_data(self, file_path):
        possible_words = []
        with open(file_path) as f:
            for line in f:
                possible_words.append(line.strip())
        return possible_words

    def refine_words(self, feedback, word):

        new_possible_words = []
        for possible_word in self.possible_words:
            match = True
            for i, (feedback_char, char) in enumerate(zip(feedback, word)):
                if feedback_char == 'G' and possible_word[i] != char:
                    match = False
                    break
                elif feedback_char == 'Y' and (char not in possible_word or possible_word[i] == char):
                    match = False
                    break
                elif feedback_char == 'R' and char in possible_word:
                    match = False
                    break
            if match:
                new_possible_words.append(possible_word)

        self.possible_words = new_possible_words
        return new_possible_words


class Bot:
    def __init__(self, name, file_path):
        self.mastermind_bot = Wordle_Bot(name)
        self.game = Game(file_path)
        self.guess = random.choice(self.game.possible_words)

    def register_and_create_game(self):
        self.mastermind_bot.register()
        self.mastermind_bot.create_game()

    def play_game(self):
        guess_word = self.guess
        while True:
            feedback, message = self.mastermind_bot.make_guess(guess_word)
            if feedback and "G" in feedback:
                if feedback == "GGGGG":
                    print("Guessed the correct word!")
                    break
            if feedback and feedback != "GGGGG":
                self.game.refine_words(feedback, guess_word)
                if self.game.possible_words:
                    guess_word = random.choice(self.game.possible_words)
                else:
                    print("No more possible words to guess.")
                    break
            else:
                print("No feedback received.")
                break

name = "sanika"
word_list_file = 'words.txt'
bot = Bot(name, word_list_file)
bot.register_and_create_game()
bot.play_game()
