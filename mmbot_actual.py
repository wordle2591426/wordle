import requests as rq
from typing import Self
import random
DEBUG = False

class MMBot:
    words = [word.strip() for word in open("words.txt")]
    mm_url = "https://we6.talentsprint.com/wordle/game/"
    register_url = mm_url + "register"
    creat_url = mm_url + "create"
    guess_url = mm_url + "guess"

    def __init__(self: Self, name: str):
        def is_unique(w: str) -> bool:
            return len(w) == len(set(w))

        self.session = rq.session()
        register_dict = {"mode": "wordle", "name": name}
        reg_resp = self.session.post(MMBot.register_url, json=register_dict)
        self.me = reg_resp.json()['id']
        creat_dict = {"id": self.me, "overwrite": True}
        self.session.post(MMBot.creat_url, json=creat_dict)

        self.choices = [w for w in MMBot.words[:] if is_unique(w)]
        random.shuffle(self.choices)

    def play(self: Self) -> None:
        def post(choice: str) -> str:
            guess = {"id": self.me, "guess": choice}
            response = self.session.post(MMBot.guess_url, json=guess)
            rj = response.json()
            right = rj.get("feedback", "")
            #status = "win" in rj["message"]
            return right

        choice = random.choice(self.choices)
        self.choices.remove(choice)
        right= post(choice)
        tries = [f'{choice}:{right}']

        while right != "GGGGG":
            if DEBUG:
                print(choice, right, self.choices[:10])
            self.update(choice, right)
            choice = random.choice(self.choices)
            self.choices.remove(choice)
            right = post(choice)
            tries.append(f'{choice}:{right}')
        print("Secret is", choice, "found in", len(tries), "attempts")
        print("Route is:", " => ".join(tries))
    def update(self: Self, choice: str, right: str):
        new_choices = []
        for word in self.choices:
            if self.match_feedback(choice, word, right):
                new_choices.append(word)
        self.choices = new_choices


    def match_feedback(self: Self, guess: str, word: str, right: str) -> bool:
        for i, (g, w, f) in enumerate(zip(guess, word, right)):
            if f == 'G' and g != w: 
                return False
            elif f == 'Y' and (g == w or g not in word):  
                return False
            elif f == 'B' and g in word:  
                return False
        return True
        
#        def common(choice: str, word: str):
 #           return len(set(choice) & set(word))
  #      self.choices = [w for w in self.choices if common(choice, w) == right]


 # def update(self: Self, choice: str, right: int):
    #def count_matching_positions(choice: str, word: str) -> int:
     #   return sum(1 for ch1, ch2 in zip(choice, word) if ch1 == ch2)
    #self.choices = [w for w in self.choices if count_matching_positions(choice, w) == right]




game = MMBot("CodeShifu")
game.play()

'''def red():
    response = self.session.post(MMBot.guess_url, json=guess)
    reg_resp = self.session.post(MMBot.register_url, json=register_dict)
    for ch in response:
        if ch not in reg_resp:
            red = [word in words if ch not in word]
            for _ in red:
                words.remove(_)'''
